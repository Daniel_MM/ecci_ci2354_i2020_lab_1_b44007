package cr.ac.ucr.ecci.eseg.sodauniversitaria;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    public final static String EXTRA_MESSAGE = "persona";
    private static final int SECOND_ACTIVITY_RESULT_CODE = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Persona persona = new Persona();
        final Soda soda = new Soda();

        TextView textViewPersona = findViewById(R.id.persona);
        Button buttonPagina = findViewById(R.id.buttonPagina);
        Button buttonLlamar = findViewById(R.id.buttonLlamar);
        Button buttonCorreo = findViewById(R.id.buttonCorreo);
        Button buttonMapa = findViewById(R.id.buttonMapa);
        Button buttonCalcularPropina = findViewById(R.id.buttonCalcularPropina);
        Button buttonListaSodas = findViewById(R.id.buttonListaSodas);

        String MensajeBienvenida = persona.getNombre() + getString(R.string.es_un_placer_servirle);
        textViewPersona.setText(MensajeBienvenida);

        buttonPagina.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                irPagina(soda);
            }
        });
        buttonLlamar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                realizarLlamada(soda);
            }
        });
        buttonCorreo.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                enviarCorreo(soda);
            }
        });
        buttonMapa.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                irMapa(soda);
            }
        });
        buttonCalcularPropina.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                irCalculadoraPropina(persona);
            }
        });
        buttonListaSodas.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                irListaSodas();
            }
        });
    }


    private void irPagina(Soda soda) {
        // Intent para ver una página en el browser
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(soda.getWebsite()));
        startActivity(intent);
    }

    private void realizarLlamada(Soda soda) {
        // Intent para abrir el teclado de llamada
        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + soda.getTelefono()));
        startActivity(intent);
    }

    private void enviarCorreo(Soda soda) {
        // Intent para abrir el correo
        String[] TO = {soda.getEmail()};
        Uri uri = Uri.parse("mailto:" + soda.getEmail())
                .buildUpon()
                .appendQueryParameter("subject", "Consulta Soda Universitaria")
                .appendQueryParameter("body", "Enviado desde SodaUniversitaria.")
                .build();
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, uri);
        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
        startActivity(Intent.createChooser(emailIntent, "Enviar vía correo"));
    }

    private void irMapa(Soda soda) {
        // Intent para ver la localización en el mapa
        String url = "geo:" + soda.getLatitud() + "," + soda.getLongitud();
        String q = "?q=" + soda.getLatitud() + "," + soda.getLongitud() + "(" + soda.getNombre() + ")";
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url + q));
        intent.setPackage("com.google.android.apps.maps");
        startActivity(intent);
    }


    private void irCalculadoraPropina(Persona persona) {
        // Intent para llamar a la Actividad Calculadora
        Intent intent = new Intent(this, CalculadoraActivity.class);
        intent.putExtra(EXTRA_MESSAGE, persona);
        // Deseo recibir una respuesta: startActivityForResult()
        startActivityForResult(intent, SECOND_ACTIVITY_RESULT_CODE);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // check that it is the SecondActivity with an OK result
        if (requestCode == SECOND_ACTIVITY_RESULT_CODE) {
            if (resultCode == RESULT_OK) {
                // Obtener datos del Intent
                String returnString = data.getStringExtra("montoStr");
                // mostrar la respuesta
                Toast.makeText(getApplicationContext(), returnString, Toast.LENGTH_LONG).show();
            }
        }
    }

    private void irListaSodas(){
        // Intent para llamar a la Actividad Lista sodas
        Intent intent = new Intent(this, ListaSodasActivity.class);

        startActivity(intent);
    }
}
